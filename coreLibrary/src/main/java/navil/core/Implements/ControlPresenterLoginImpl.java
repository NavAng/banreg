package navil.core.Implements;

import java.util.ArrayList;

import navil.core.Intefaces.ControlDataInterface;
import navil.core.Models.BanRegModel;
import navil.core.Models.CredentialsModel;

public class ControlPresenterLoginImpl implements ControlDataInterface.ControlPresenter, ControlDataInterface.GetLoginControl.OnFinishedListener, ControlDataInterface.LoginView {

    private ControlDataInterface.LoginView loginView;
    private ControlDataInterface.GetLoginControl  getLoginControl;

    public ControlPresenterLoginImpl(ControlDataInterface.LoginView loginView, ControlDataInterface.GetLoginControl getLoginControl) {
        this.loginView = loginView;
        this.getLoginControl = getLoginControl;
    }


    @Override
    public void onDestroy() {
        loginView = null;
    }

    @Override
    public void onRefreshButtonClick() {

        if(loginView != null){
            loginView.showProgress();
        }
        getLoginControl.getLoginControl(this);

    }

    @Override
    public void requestDataFromServer() {
        getLoginControl.getLoginControl(this);
    }

    @Override
    public void onFinished(CredentialsModel CredentialsModel) {
        if(loginView != null){
            loginView.setDataCredentials(CredentialsModel);
            loginView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(loginView != null){
            loginView.onResponseFailure(t);
            loginView.hideProgress();
        }
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void setDataCredentials(CredentialsModel credentialsModel) {

    }

    @Override
    public void onResponseFailure(Throwable throwable) {

    }
}
