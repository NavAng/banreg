package navil.core.Implements;

import android.util.Log;

import java.util.ArrayList;

import navil.core.Connection.BanRegApi;
import navil.core.Intefaces.ControlDataInterface;
import navil.core.Models.BanRegModel;
import navil.core.Network.RetrofitProxy;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetSucursalsControlImpl implements ControlDataInterface.GetSucusalsControl{


    @Override
    public void getSucursalsArrayList(final OnFinishedListener onFinishedListener) {

        /** Create handle for the RetrofitInstance interface*/
        /** Call the method with parameter in the interface to get the notice data*/
        BanRegApi service = RetrofitProxy.getRetrofitInstance().create(BanRegApi.class);
        Call<ArrayList<BanRegModel>> call = service.getSucursales();


        call.enqueue(new Callback<ArrayList<BanRegModel>>() {
            @Override
            public void onResponse(Call<ArrayList<BanRegModel>> call, Response<ArrayList<BanRegModel>> response) {
                onFinishedListener.onFinished(response.body());

            }

            @Override
            public void onFailure(Call<ArrayList<BanRegModel>>call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
