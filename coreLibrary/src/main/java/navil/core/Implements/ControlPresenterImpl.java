package navil.core.Implements;

import java.util.ArrayList;

import navil.core.Intefaces.ControlDataInterface;
import navil.core.Models.BanRegModel;

public class ControlPresenterImpl implements ControlDataInterface.ControlPresenter, ControlDataInterface.GetSucusalsControl.OnFinishedListener {

    private ControlDataInterface.ControlView controlView;
    private ControlDataInterface.GetSucusalsControl  getSucusalsControl;

    public ControlPresenterImpl(ControlDataInterface.ControlView controlView, ControlDataInterface.GetSucusalsControl getSucusalsControl) {
        this.controlView = controlView;
        this.getSucusalsControl = getSucusalsControl;
    }


    @Override
    public void onDestroy() {
        controlView = null;
    }

    @Override
    public void onRefreshButtonClick() {

        if(controlView != null){
            controlView.showProgress();
        }
        getSucusalsControl.getSucursalsArrayList(this);

    }

    @Override
    public void requestDataFromServer() {
        getSucusalsControl.getSucursalsArrayList(this);
    }

    @Override
    public void onFinished(ArrayList<BanRegModel> noticeArrayList) {
        if(controlView != null){
            controlView.setDataToRecyclerView(noticeArrayList);
            controlView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(controlView != null){
            controlView.onResponseFailure(t);
            controlView.hideProgress();
        }
    }
}