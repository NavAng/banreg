package navil.core.Implements;

import android.util.Log;

import navil.core.Connection.BanRegApi;
import navil.core.Intefaces.ControlDataInterface;
import navil.core.Models.CredentialsModel;
import navil.core.Network.RetrofitProxy;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetLoginControlImpl implements ControlDataInterface.GetLoginControl{

    @Override
    public void getLoginControl(final OnFinishedListener onFinishedListener) {
        /** Create handle for the RetrofitInstance interface*/
        /** Call the method with parameter in the interface to get the notice data*/
        BanRegApi service = RetrofitProxy.getRetrofitInstance().create(BanRegApi.class);
        Call<CredentialsModel> call = service.getLogin();

        call.enqueue(new Callback<CredentialsModel>() {
            @Override
            public void onResponse(Call<CredentialsModel> call, Response<CredentialsModel> response) {
                onFinishedListener.onFinished(response.body());

            }

            @Override
            public void onFailure(Call<CredentialsModel> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
