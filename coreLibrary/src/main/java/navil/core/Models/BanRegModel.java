package navil.core.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class BanRegModel implements Parcelable {
    @SerializedName("ID")
    public String strId;
    @SerializedName("tipo")
    public String strType;
    @SerializedName("NOMBRE")
    public String strName;
    @SerializedName("DOMICILIO")
    public String strAddress;
    @SerializedName("HORARIO")
    public String strSchedule;
    @SerializedName("TELEFONO_PORTAL")
    public String strPhonePortal;
    @SerializedName("TELEFONO_APP")
    public String strPhoneApp;
    @SerializedName("24_HORAS")
    public String strTwentyfour;
    @SerializedName("SABADOS")
    public String strSaturdays;
    @SerializedName("CIUDAD")
    public String strCity;
    @SerializedName("ESTADO")
    public String strState;
    @SerializedName("Latitud")
    public String strLatitude;
    @SerializedName("Longitud")
    public String strLongitude;
    @SerializedName("Correo_Gerente")
    public String strEmailManager;
    @SerializedName("URL_FOTO")
    public String strUrlPicture;
    @SerializedName("Suc_Estado_Prioridad")
    public String strBranchStatePriority;
    @SerializedName("Suc_Ciudad_Prioridad")
    public String strBranchCityPriority;

    public static BanRegModel fromJson(String strJsonBanRegModel) {
        //                                                      //fromJSON of Persistent Data

        //                                                      //GSON use the parcel funtion
        return new Gson().fromJson(strJsonBanRegModel, BanRegModel.class);
    }
    public String toString() {
        //                                                      //Transform Object to JSON
        return new Gson().toJson(this);
    }
    protected BanRegModel(Parcel in) {
        //                                                      //Parcel Data
        strId = in.readString();
        strType = in.readString();
        strName = in.readString();
        strAddress = in.readString();
        strSchedule = in.readString();
        strPhonePortal = in.readString();
        strPhoneApp = in.readString();
        strTwentyfour = in.readString();
        strSaturdays = in.readString();
        strCity = in.readString();
        strState = in.readString();
        strLatitude = in.readString();
        strLongitude = in.readString();
        strEmailManager = in.readString();
        strUrlPicture = in.readString();
        strBranchStatePriority = in.readString();
        strBranchCityPriority = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        //                                                  //Write Parcel Data
        dest.writeString(strId);
        dest.writeString(strType);
        dest.writeString(strName);
        dest.writeString(strAddress);
        dest.writeString(strSchedule);
        dest.writeString(strPhonePortal);
        dest.writeString(strPhoneApp);
        dest.writeString(strTwentyfour);
        dest.writeString(strSaturdays);
        dest.writeString(strCity);
        dest.writeString(strState);
        dest.writeString(strLatitude);
        dest.writeString(strLongitude);
        dest.writeString(strEmailManager);
        dest.writeString(strUrlPicture);
        dest.writeString(strBranchStatePriority);
        dest.writeString(strBranchCityPriority);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BanRegModel> CREATOR = new Creator<BanRegModel>() {
        @Override
        public BanRegModel createFromParcel(Parcel in) {
            return new BanRegModel(in);
        }

        @Override
        public BanRegModel[] newArray(int size) {
            return new BanRegModel[size];
        }
    };
}
