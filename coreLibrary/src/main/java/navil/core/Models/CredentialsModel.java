package navil.core.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CredentialsModel implements Parcelable {
    @SerializedName("user")
    public String strUser;
    @SerializedName("password")
    public String strPass;

    protected CredentialsModel(Parcel in) {
        strUser = in.readString();
        strPass = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(strUser);
        dest.writeString(strPass);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CredentialsModel> CREATOR = new Creator<CredentialsModel>() {
        @Override
        public CredentialsModel createFromParcel(Parcel in) {
            return new CredentialsModel(in);
        }

        @Override
        public CredentialsModel[] newArray(int size) {
            return new CredentialsModel[size];
        }
    };
}
