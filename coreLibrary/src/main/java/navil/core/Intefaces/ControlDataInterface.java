package navil.core.Intefaces;

import android.location.LocationManager;

import java.util.ArrayList;
import java.util.List;

import navil.core.Models.BanRegModel;
import navil.core.Models.CredentialsModel;

public interface ControlDataInterface {


    interface ControlPresenter {

        void onDestroy();

        void onRefreshButtonClick();

        void requestDataFromServer();

    }
    public interface SearchPresenter {
        public void updateSearch(final String query);

        public void submitSearch(final String query);

        public void close();
    }
    interface ControlView {

        void showProgress();

        void hideProgress();

        void setDataToRecyclerView(ArrayList<BanRegModel> banRegModelArrayList);

        void onResponseFailure(Throwable throwable);

    }
    interface LoginView {

        void showProgress();

        void hideProgress();

        void setDataCredentials(CredentialsModel credentialsModel);

        void onResponseFailure(Throwable throwable);

    }
    public interface SearchView {
        public void close();

        public void clearSuggestions();

        public void showSuggestions(final List<String> suggestions);
    }


    interface GetSucusalsControl {

        interface OnFinishedListener {
            void onFinished(ArrayList<BanRegModel> banRegModelArrayList);

            void onFailure(Throwable t);
        }

        void getSucursalsArrayList(OnFinishedListener onFinishedListener);
    }
    interface GetResultSearch {

        interface OnFinishedListener {
            void onResult(ArrayList<BanRegModel> banRegModelArrayList);

        }

        void getResulSearch(OnFinishedListener onFinishedListener, ArrayList<BanRegModel> banRegModelArrayList);
    }
    interface GetLoginControl {

        interface OnFinishedListener {
            void onFinished(CredentialsModel credentialsModel);

            void onFailure(Throwable t);
        }

        void getLoginControl(OnFinishedListener onFinishedListener);
    }
}