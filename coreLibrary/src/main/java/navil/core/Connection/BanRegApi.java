package navil.core.Connection;

import java.util.ArrayList;

import navil.core.Models.BanRegModel;
import navil.core.Models.CredentialsModel;
import retrofit2.Call;
import retrofit2.http.GET;

public interface BanRegApi {
    //                                                      //Dictionary of EndPoints
    String ENDPOINT = "http://json.banregio.io/";
    //                                                      //WS LOGIN (Credenciales)
    @GET("sucursales")
    Call<ArrayList<BanRegModel>> getSucursales();
    //                                                      //WS SUCURSALES (BRANCH TYPE C & S)
    @GET("login")
    Call<CredentialsModel> getLogin();

}
