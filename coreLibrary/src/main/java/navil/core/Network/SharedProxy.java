package navil.core.Network;

import android.content.Context;

import navil.core.Security.SecurePreferences;

public class SharedProxy {

    public static SecurePreferences preferences;
    /**
     * Create an instance of SecurePreferences
     * */
    public static SecurePreferences getSaredInstance(Context context)
    {
        //                                                  //Instancia de Secure para almacenar data
        preferences = new SecurePreferences(context ,"preferences", "SecretAppkey", true);
        return preferences;
    }
}
