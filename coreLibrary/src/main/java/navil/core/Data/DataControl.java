package navil.core.Data;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import navil.core.Models.BanRegModel;
import navil.core.Network.SharedProxy;
import navil.core.Security.SecurePreferences;

public class DataControl {

    //                                                      //Percistencia de Sucursales
    public static void saveAll(Context context,ArrayList<BanRegModel> banRegModelArrayList) throws JSONException {
        SecurePreferences preferences= SharedProxy.getSaredInstance(context);
        JSONArray jsonArrayBr=new JSONArray();
        for (BanRegModel model:
             banRegModelArrayList) {
            jsonArrayBr.put(new JSONObject(model.toString()));
        }
        preferences.put("banregarray",jsonArrayBr.toString());
    }
    public static ArrayList<BanRegModel> getAll(Context context) throws JSONException {
        SecurePreferences preferences= SharedProxy.getSaredInstance(context);
        ArrayList<BanRegModel> arrayList=new ArrayList<>();
        if(preferences.getString("banregarray")!=null)
        {
            JSONArray jsonArray= new JSONArray(preferences.getString("banregarray"));
            for(int i=0;i<jsonArray.length();i++)
            {
                arrayList.add(BanRegModel.fromJson(jsonArray.optJSONObject(i).toString()));

            }


        }
        return  arrayList;
    }
}
