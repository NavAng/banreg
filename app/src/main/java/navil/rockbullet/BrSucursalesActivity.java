package navil.rockbullet;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;

import java.util.ArrayList;

import navil.core.Data.DataControl;
import navil.core.Implements.ControlPresenterImpl;
import navil.core.Implements.GetSucursalsControlImpl;
import navil.core.Intefaces.ControlDataInterface;
import navil.core.Models.BanRegModel;
import navil.rockbullet.Fragments.BrDetailSucursalDialogFragment;

public class BrSucursalesActivity extends FragmentActivity implements OnMapReadyCallback, ControlDataInterface.ControlView {

    private GoogleMap mMap;
    private Button btnBrSearch;
    private RelativeLayout progress;
    private final String strTYPE_BRANCHO = "S";
    private final String strTYPE_ATM = "C";
    private ControlDataInterface.ControlPresenter controlPresenter;
    private ArrayList<BanRegModel> banRegModelArrayList;

    private View.OnClickListener btnBrSearchOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intentBrSuctoSea = new Intent(BrSucursalesActivity.this, BrSearchActivity.class);
            Bundle bData = new Bundle();
            bData.putParcelableArrayList("list", banRegModelArrayList);
            intentBrSuctoSea.putExtras(bData);
            startActivity(intentBrSuctoSea);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sucursales_content);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        btnBrSearch = findViewById(R.id.btn_search);
        progress=findViewById(R.id.progress);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        btnBrSearch.setOnClickListener(btnBrSearchOnClickListener);
        mapFragment.getMapAsync(this);
        controlPresenter = new ControlPresenterImpl(this, new GetSucursalsControlImpl());
        controlPresenter.requestDataFromServer();

    }


    //                                                      Pendiente por pasar a librería
    @SuppressLint("MissingPermission")
    private void validatePermissions() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            int PERMISSION_ALL = 1;
            String[] PERMISSIONS = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION};

            if (!hasPermissions(this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            } else {
                String provider = getProviderName();
                if (provider.equalsIgnoreCase(LocationManager.GPS_PROVIDER)) {
                     Location fastLocation = locationManager.getLastKnownLocation(provider);
                    if (fastLocation != null) {
                        latitude = fastLocation.getLatitude();
                        longitude = fastLocation.getLongitude();
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));
                    } else {
                      Location fastLocationNetw = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (fastLocationNetw != null) {
                            latitude = fastLocationNetw.getLatitude();
                            longitude = fastLocationNetw.getLongitude();
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));

                        }
                    }

                } else if (provider.equalsIgnoreCase(LocationManager.NETWORK_PROVIDER)) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    Location fastLocation = locationManager.getLastKnownLocation(provider);

                    if (fastLocation != null) {
                        latitude = fastLocation.getLatitude();
                        longitude = fastLocation.getLongitude();
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));
                    } else {
                        Location fastLocationNetw = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (fastLocationNetw != null) {
                            latitude = fastLocationNetw.getLatitude();
                            longitude = fastLocationNetw.getLongitude();
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));
                        }
                    }
                }
            }
        }else
        {
            String provider = getProviderName();

            if(provider.equalsIgnoreCase(LocationManager.GPS_PROVIDER))
            {
                Location fastLocation = locationManager.getLastKnownLocation(provider);
                if (fastLocation != null) {
                    latitude = fastLocation.getLatitude();
                    longitude = fastLocation.getLongitude();
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));
                }
                else
                {
                    Location fastLocationNetw = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (fastLocationNetw != null) {
                        latitude = fastLocationNetw.getLatitude();
                        longitude = fastLocationNetw.getLongitude();
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));
                    }
                }
            }
            else if(provider.equalsIgnoreCase(LocationManager.NETWORK_PROVIDER)) {
                Location fastLocation = locationManager.getLastKnownLocation(provider);
                if (fastLocation != null) {
                    latitude = fastLocation.getLatitude();
                    longitude = fastLocation.getLongitude();
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));
                } else {
                    Location fastLocationNetw = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (fastLocationNetw != null) {
                        latitude = fastLocationNetw.getLatitude();
                        longitude = fastLocationNetw.getLongitude();
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));
                    }
                }
            }

        }
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        validatePermissions();
        // Add a marker in Sydney and move the camera


    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void setDataToRecyclerView(ArrayList<BanRegModel> banRegModelArrayList) {
        this.banRegModelArrayList=banRegModelArrayList;
        try {
            DataControl.saveAll(this,this.banRegModelArrayList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (BanRegModel banRegModel:
                banRegModelArrayList) {

            MarkerOptions marker = new MarkerOptions().position(
                    new LatLng(Double.parseDouble(banRegModel.strLatitude),
                            Double.parseDouble(banRegModel.strLongitude)))
                    .title(banRegModel.strName);
            if(banRegModel.strType.equalsIgnoreCase(strTYPE_BRANCHO))
            {
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.sucursal));
            }
            else
            {
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.atm));
            }

           mMap.addMarker(marker).setTag(banRegModel);
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    FragmentManager fm = getSupportFragmentManager();
                    BrDetailSucursalDialogFragment dialogFragmentsStatsDetail = new BrDetailSucursalDialogFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("location", (Parcelable) marker.getTag());
                    dialogFragmentsStatsDetail.setArguments(bundle);
                    dialogFragmentsStatsDetail.show(fm, BrDetailSucursalDialogFragment.class.getCanonicalName());
                    return false;
                }
            });

// adding marker

        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        progress.setVisibility(View.GONE);
        try {
            this.banRegModelArrayList=DataControl.getAll(this);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public Double latitude,longitude;

    LocationManager locationManager;
    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

            switch (requestCode) {
                case 1: {
                    String provider = getProviderName();
                    if(provider!=null) {
                        if (provider.equalsIgnoreCase(LocationManager.GPS_PROVIDER)) {
                            Location fastLocation = locationManager.getLastKnownLocation(provider);
                            if (fastLocation != null) {
                                latitude = fastLocation.getLatitude();
                                longitude = fastLocation.getLongitude();
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));

                            } else {
                                Location fastLocationNetw = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                if (fastLocationNetw != null) {
                                    latitude = fastLocationNetw.getLatitude();
                                    longitude = fastLocationNetw.getLongitude();
                                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));
                                }
                            }

                        } else if (provider.equalsIgnoreCase(LocationManager.NETWORK_PROVIDER)) {
                            Location fastLocation = locationManager.getLastKnownLocation(provider);
                            if (fastLocation != null) {
                                latitude = fastLocation.getLatitude();
                                longitude = fastLocation.getLongitude();
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));
                            } else {
                                Location fastLocationNetw = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                if (fastLocationNetw != null) {
                                    latitude = fastLocationNetw.getLatitude();
                                    longitude = fastLocationNetw.getLongitude();
                                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 12.0f));
                                }
                            }
                        }

                    }
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                }
            }
        } else {
        }

        return;



    }
    String getProviderName() {
        LocationManager locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.POWER_LOW); // Chose your desired power consumption level.
        criteria.setAccuracy(Criteria.ACCURACY_FINE); // Choose your accuracy requirement.
        criteria.setSpeedRequired(true); // Chose if speed for first location fix is required.
        criteria.setAltitudeRequired(false); // Choose if you use altitude.
        criteria.setBearingRequired(false); // Choose if you use bearing.
        criteria.setCostAllowed(false); // Choose if this provider can waste money :-)

        // Provide your criteria and flag enabledOnly that tells
        // LocationManager only to return active providers.
        return locationManager.getBestProvider(criteria, true);
    }
    public  boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

}
