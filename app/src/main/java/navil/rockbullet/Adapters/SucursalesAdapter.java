package navil.rockbullet.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import navil.core.Models.BanRegModel;
import navil.rockbullet.R;

public class SucursalesAdapter extends BaseAdapter {
    private Context mContext;
    private final ArrayList<BanRegModel> banRegModelArrayList;


    public SucursalesAdapter(Context c, ArrayList<BanRegModel> banRegModelArrayList) {
        this.mContext = c;
        this.banRegModelArrayList = banRegModelArrayList;

    }

    private class ViewHolder {
        TextView tvTitle,tvAddress;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return banRegModelArrayList.size();
    };

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return banRegModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View v = convertView;
        ViewHolder holder ;


        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(R.layout.item_list_uno, null);
            holder=new ViewHolder();

            holder.tvTitle=v.findViewById(R.id.tv_namew);
            holder.tvAddress=v.findViewById(R.id.tv_addressw);

            v.setTag(holder);

        }
        else {
            holder = (ViewHolder) v.getTag();
        }



        try {
            holder.tvTitle.setText(banRegModelArrayList.get(position).strName);
            holder.tvAddress.setText(banRegModelArrayList.get(position).strAddress);


        }catch(Exception ex){
            ex.printStackTrace();
        }

        return v;
    }

}