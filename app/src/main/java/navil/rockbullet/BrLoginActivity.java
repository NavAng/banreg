package navil.rockbullet;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import navil.core.Implements.ControlPresenterLoginImpl;
import navil.core.Implements.GetLoginControlImpl;
import navil.core.Intefaces.ControlDataInterface;
import navil.core.Models.CredentialsModel;

public class BrLoginActivity extends AppCompatActivity implements ControlDataInterface.LoginView,View.OnClickListener {
    private RelativeLayout progress;

    private ControlDataInterface.ControlPresenter loginPresenter;
    private Button btnLogin;
    private EditText etUser,etPassword;
    private View.OnClickListener btnLoginOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            loginPresenter.requestDataFromServer();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initUI();
        loginPresenter= new ControlPresenterLoginImpl(this, new GetLoginControlImpl());

    }
    private void initUI()
    {
        btnLogin=findViewById(R.id.btn_login);
        etUser=findViewById(R.id.et_user);
        etPassword=findViewById(R.id.et_password);
        progress=findViewById(R.id.progress);
        btnLogin.setOnClickListener(btnLoginOnClickListener);
    }
    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void setDataCredentials(CredentialsModel credentialsModel) {
    if(credentialsModel!=null)
    {
        if( //                                              //Validate EditText Longitude
                etPassword.getText().toString().length()>0 &&
                        etUser.getText().toString().length()>0
                )
        {
            if( //                                          //Validate Credentials
                    etPassword.getText().toString().equalsIgnoreCase(credentialsModel.strPass) &&
                            etPassword.getText().toString().
                                    equalsIgnoreCase(credentialsModel.strPass)
                    )
            {
                Intent intentBRLtoBRS= new Intent(this,BrSucursalesActivity.class);
                startActivity(intentBRLtoBRS);
            }
            else
            {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(this);
                }
                builder.setTitle("Conexión")
                        .setMessage("El usuario o la contraseña no es la correcta.")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }
    }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {

    }


}
