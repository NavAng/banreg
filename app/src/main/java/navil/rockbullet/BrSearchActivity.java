package navil.rockbullet;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import navil.core.Models.BanRegModel;
import navil.rockbullet.Adapters.SucursalesAdapter;
import navil.rockbullet.Fragments.BrDetailSucursalDialogFragment;

public class BrSearchActivity extends AppCompatActivity {
    private ArrayList<BanRegModel> listBanRegModels;
    private ListView lvBrSucursalsFound;
    private SucursalesAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_br_search);
        lvBrSucursalsFound=findViewById(R.id.lv_search);

        if(getIntent().getExtras().getParcelableArrayList("list")!=null)
        {
            listBanRegModels =getIntent().getExtras().getParcelableArrayList("list");
            adapter=new SucursalesAdapter(this,listBanRegModels);
            lvBrSucursalsFound.setAdapter(adapter);
            lvBrSucursalsFound.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    FragmentManager fm = getSupportFragmentManager();
                    BrDetailSucursalDialogFragment dialogFragmentsStatsDetail = new BrDetailSucursalDialogFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("location", listBanRegModels.get(i));
                    dialogFragmentsStatsDetail.setArguments(bundle);
                    dialogFragmentsStatsDetail.show(fm, BrDetailSucursalDialogFragment.class.getCanonicalName());
                }
            });
        }
    }
}
