package navil.rockbullet.Fragments;

import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import navil.core.Models.BanRegModel;
import navil.rockbullet.R;


/**
 * Created by navil on 15/05/18.
 */

public class BrDetailSucursalDialogFragment extends DialogFragment {


    private final String strTYPE_BRANCHO="S";
    private final String strTYPE_ATM="C";
    private BanRegModel banRegModel;
    private TextView tvBrAddress,tvBrPhonePortal,tvBrPhoneApp,tvBrSchedule,tvBrSaturday,tvBr24hours, tvBrTitle,
            tvBrTshedule,tvBrTphoneApp,tvBrTphonePortal;
    private ImageView ivBrPic;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View  view= inflater.inflate(R.layout.detail_sucursal_dfragment, container, false);
        initUI(view);
        banRegModel=getArguments().getParcelable("location");
        fillData(banRegModel);
        Log.e("DETAIL FROM: ", banRegModel.strName + " address: "+banRegModel.strType);
        return view;
    }
    private void fillData(BanRegModel banRegModel)
    {
        tvBrAddress.setText(banRegModel.strAddress);
        tvBrPhonePortal.setText(banRegModel.strPhonePortal);
        tvBrPhoneApp.setText(banRegModel.strPhoneApp);
        tvBrSchedule.setText(banRegModel.strSchedule);
        tvBrSaturday.setText(banRegModel.strSaturdays);
        tvBr24hours.setText(banRegModel.strTwentyfour);
        tvBrTitle.setText(strConfigType(banRegModel.strName,banRegModel.strType));
    }
    private String strConfigType(String name, String type)
    {
        String title="";
        if(type.equalsIgnoreCase(strTYPE_BRANCHO))
        {
            title=  "SUCURSAL - "+name;
            ivBrPic.setImageResource(R.drawable.detail_sucursal);
        }
        else
        {
            title="CAJERO - "+name;
            ivBrPic.setImageResource(R.drawable.detail_atm);
            tvBrTshedule.setVisibility(View.GONE);
            tvBrSchedule.setVisibility(View.GONE);
            tvBrTphonePortal.setVisibility(View.GONE);
            tvBrPhonePortal.setVisibility(View.GONE);
            tvBrTphoneApp.setVisibility(View.GONE);
            tvBrPhoneApp.setVisibility(View.GONE);

        }
        return title;
    }
    private void initUI(View view)
    {
        tvBrAddress = view.findViewById(R.id.tv_address);
        tvBrPhonePortal = view.findViewById(R.id.tv_phone_portal);
        tvBrPhoneApp = view.findViewById(R.id.tv_phone_app);
        tvBrSchedule = view.findViewById(R.id.tv_schedule);
        tvBrSaturday = view.findViewById(R.id.tv_saturdays);
        tvBr24hours = view.findViewById(R.id.tv_24);
        tvBrTphoneApp=view.findViewById(R.id.tv_tapp);
        tvBrTphonePortal=view.findViewById(R.id.tv_tportal);
        tvBrTshedule=view.findViewById(R.id.tv_tschedule);
        tvBrTitle=view.findViewById(R.id.tv_title);
        ivBrPic=view.findViewById(R.id.iv_pic);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar);

    }




    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        return dialog;
    }









}
